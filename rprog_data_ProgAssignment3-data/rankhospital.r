rankhospital <- function(state, criteria, num = "best") {
  outcome <- read.csv("outcome-of-care-measures.csv", header=TRUE, stringsAsFactors=F, na.strings="Not Available")
  
  if (!state %in% outcome[,7]) {
    stop("State error")
  }
  
  if (criteria == "heart attack") {
    column <- 11  
  } else if (criteria == "heart failure") {
    column <- 17
  } else if (criteria == "pneumonia") {
    column <- 23
  } else {
    stop("Criteria error")
  }
  
  statedata <- subset(outcome, State == state)
  
  if (nrow(statedata) < num) {
    return(NA)
  } else if (num == "best") {
    mindata <- min(statedata[,column], na.rm = TRUE)
    minindex <- which(statedata[,column] == mindata)
    hospitalname = statedata[minindex,2]
    return(hospitalname)
  } else if (num == "worst") {
    maxdata <- max(statedata[,column], na.rm = TRUE)
    maxindex <- which(statedata[,column] == maxdata)
    hospitalname <- statedata[maxindex,2]
    return(hospitalname)
  } else {
    statedata <- statedata[order(statedata[,column], statedata[,2]), ]
    hospitalname <- statedata[num, 2]
    return(hospitalname)
  }
  
}