corr <- function(directory, threshold = 0) {
  ## 'directory' is a character vector of length 1 indicating
  ## the location of the CSV files
  
  ## 'threshold' is a numeric vector of length 1 indicating the
  ## number of completely observed observations (on all
  ## variables) required to compute the correlation between
  ## nitrate and sulfate; the default is 0
  
  ## Return a numeric vector of correlations
  ## NOTE: Do not round the result!
  numoffiles <- length(list.files(directory))
  nobswithid <- complete(directory, 1:numoffiles)
  aboveth <- nobswithid[,2] > threshold

  output <- numeric(sum(aboveth))
  counter <- 1

  for (i in 1:numoffiles) {
    if (nobswithid[i, 2] > threshold) {
      if (i< 10) {
        data = read.csv(paste0(directory, "/00", i, ".csv"))
      } else if (i>=10 && i <100) {
        data = read.csv(paste0(directory, "/0", i, ".csv"))
      } else {
        data = read.csv(paste0(directory, "/", i, ".csv"))
      }
      filtereddata <- data[complete.cases(data), ]
      output[counter] <- cor(filtereddata[,2], filtereddata[, 3])    
      counter = counter + 1
    }
  }
  output
}